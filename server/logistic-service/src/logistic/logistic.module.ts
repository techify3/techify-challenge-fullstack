import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { LogisticController } from './logistic.controller';
import { LogisticRepository } from './logistic.repository';
import { LogisticService } from './logistic.service';
import { AuthServiceProxy, ProductServiceProxy } from './proxies';

@Module({
  imports: [ConfigModule.forRoot()],
  controllers: [LogisticController],
  providers: [
    LogisticService,
    LogisticRepository,
    AuthServiceProxy,
    ProductServiceProxy,
  ],
})
export class LogisticModule {}
