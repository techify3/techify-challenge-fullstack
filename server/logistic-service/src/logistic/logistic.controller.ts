import { Controller, Get, Post, UseGuards } from '@nestjs/common';
import { Public } from './decorators/public.decorator';
import { JwtGuard } from './guards/jwt.guard';
import { LogisticService } from './logistic.service';

@Controller('logistic')
@UseGuards(JwtGuard)
export class LogisticController {
  constructor(private readonly service: LogisticService) {}

  @Public()
  @Get()
  public async Health(): Promise<string> {
    return `logistic-service ${Date.now()}`;
  }
}
