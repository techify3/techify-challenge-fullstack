import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';

import { Reflector } from '@nestjs/core';
import { ProductRepository } from '../product.repository';

@Injectable()
export class JwtGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly productRepository: ProductRepository,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    try {
      const isPublic: boolean = this.reflector.get(
        'isPublic',
        context.getHandler(),
      );
      const isRpc: boolean = this.reflector.get('isRpc', context.getHandler());
      if (isPublic || isRpc) {
        return true;
      }
      const request = context.switchToHttp().getRequest();
      const authorization = request.headers.authorization;

      if (!authorization || !authorization.includes('Bearer')) {
        throw new UnauthorizedException();
      }

      const token = authorization.split(' ')[1];
      request.user = await this.productRepository.verify(token);
      return true;
    } catch (err) {
      throw new UnauthorizedException();
    }
  }
}
