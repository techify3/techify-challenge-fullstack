import { Controller, Get, Post, UseGuards } from '@nestjs/common';
import { Public } from './decorators/public.decorator';
import { JwtGuard } from './guards/jwt.guard';
import { ProductService } from './product.service';

@Controller('product')
@UseGuards(JwtGuard)
export class ProductController {
  constructor(private readonly service: ProductService) {}

  @Public()
  @Get()
  public async Health(): Promise<string> {
    return `product-service ${Date.now()}`;
  }
}
