import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { catchError, timeout } from 'rxjs/operators';

@Injectable()
export class ProductRepository {
  constructor(
    @Inject('auth-service') private readonly authService: ClientProxy,
    @Inject('logistic-service') private readonly logisticService: ClientProxy,
  ) {}

  public async verify(token: string) {
    try {
      return await this.authService
        .send('VERIFY', token)
        .pipe(timeout(10000))
        .pipe(
          catchError(err => {
            throw new Error(err.message);
          }),
        )
        .toPromise();
    } catch (err) {
      console.log(err);
      throw new Error(err);
    }
  }
}
