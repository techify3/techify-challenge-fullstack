import { SetMetadata, UnauthorizedException } from '@nestjs/common';

export const Test = () => {
  if (process.env.NODE_ENV === 'production') {
    throw new UnauthorizedException();
  }
  return SetMetadata('test', true);
};
