import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ProductController } from './product.controller';
import { ProductRepository } from './product.repository';
import { ProductService } from './product.service';
import { AuthServiceProxy, LogisticServiceProxy } from './proxies';

@Module({
  imports: [ConfigModule.forRoot()],
  controllers: [ProductController],
  providers: [
    ProductService,
    ProductRepository,
    AuthServiceProxy,
    LogisticServiceProxy,
  ],
})
export class ProductModule {}
