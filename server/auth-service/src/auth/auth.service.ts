import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { createHmac } from 'crypto';
import { AuthRepository } from './auth.repository';
import { SignIn } from './dto/signIn.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly repository: AuthRepository,
    private readonly jwtService: JwtService,
  ) {}

  public async verify(token: string) {
    try {
      const payload = await this.jwtService.verify(token);
      return payload;
    } catch (err) {
      throw new Error(err);
    }
  }
}
