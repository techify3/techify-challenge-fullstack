import { ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';

export const LogisticServiceProxy = {
  provide: 'logistic-service',
  inject: [ConfigService],
  useFactory: (configService: ConfigService) =>
    ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: configService.get('LOGISTIC_HOST'),
        port: configService.get('LOGISTIC_PORT'),
      },
    }),
};

export const ProductServiceProxy = {
  provide: 'product-service',
  inject: [ConfigService],
  useFactory: (configService: ConfigService) =>
    ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: configService.get('PRODUCT_HOST'),
        port: configService.get('PRODUCT_PORT'),
      },
    }),
};
