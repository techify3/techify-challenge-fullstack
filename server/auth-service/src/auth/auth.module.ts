import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { AuthRepository } from './auth.repository';
import { AuthService } from './auth.service';
import { ProductServiceProxy, LogisticServiceProxy } from './proxies';

@Module({
  imports: [
    ConfigModule.forRoot(),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '7d' },
    }),
  ],

  providers: [
    AuthService,
    AuthRepository,
    ProductServiceProxy,
    LogisticServiceProxy,
  ],
  controllers: [AuthController],
})
export class AuthModule {}
