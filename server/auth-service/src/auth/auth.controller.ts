import { Controller, Get, UseGuards } from '@nestjs/common';
import { MessagePattern, RpcException } from '@nestjs/microservices';
import { AuthService } from './auth.service';
import { RPC } from './constants';
import { Public } from './decorators/public.decorator';
import { Rpc } from './decorators/rpc.decorator';
import { JwtGuard } from './guards/jwt.guard';

@Controller('auth')
@UseGuards(JwtGuard)
export class AuthController {
  constructor(private readonly service: AuthService) {}

  @Public()
  @Get()
  public async Health() {
    return `auth-service ${Date.now()}`;
  }

  @Rpc()
  @MessagePattern(RPC.VERIFY)
  public async verify(token: string) {
    try {
      return await this.service.verify(token);
    } catch (err) {
      throw new RpcException(err);
    }
  }
}
