# Techify - Fullstack challenge 2023

### Descripción

En el siguiente challenge deberéis desarrollar una webapp la cual permita gestionar una flota de paquetes. Cada paquete en circulacion debe tener un estado con el que se pueda identificar el punto del recorrido en el que esta y una direccion de destino aleatoria. Cada paquete puede tener N cantidad de articulos hasta superar los 5KG de peso. Cada articulo tendra un peso aleatorio. En caso de superar los 5KG, tal articulo debera figurar como CANCELADO en el paquete. Los articulos pueden ser de tipo NORMAL o PREMIUM. Aquellos articulos PREMIUM deberan ser enviados al deposito para ser verificados de manera manual. En caso de aprobarse dicha verificacion, el paquete continuara hasta ser entregado. Caso contrario, el paquete volvera al remitente. En todos los casos, los paquetes y articulos deberan contar con un log que determine la hora y observacion correspondiete. Para este challenge te brindaremos la estructura común del proyecto (Opcional) y tu desarrollarás el resto de la solución.

### Requisitos

- Desarrollar el frontend utilizando NextJS
- Desarrollar el backend utilizando NestJS
- Utilizar como base de datos MongoDB
- Seguir los lineamientos SOLID

### Plus

- Utilizar MaterialUI en el frontend
- Desarrollar las pruebas necesarias utilizando Jest en el backend
- Desarrollar las pruebas necesarias utilizando Jest en el frontend
- Implementar un pipeline de CI/CD
- Deployar el proyecto utilizando AWS
- Cualquier otra mejora planteada por el desarrollador

### Recompensa

El candidato seleccionado accederá a un sueldo inicial de 50.000€ a 60.000€ anuales y se convertirá en el head of development de la startup, teniendo la posibilidad de armar y contratar a su propio equipo. #WeAreTechify

### Limitaciones

El challenge tiene un tiempo máximo de entrega de 5 días. Se tendrá en cuenta tanto la velocidad del desarrollo como las buenas practicas.

### Entrega

Enviar a bruno@wearetechify.com el repositorio final con el asunto "TECHIFY - CHALLENGE - NOMBRE APELLIDO"
