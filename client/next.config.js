/** @type {import('next').NextConfig} */
module.exports = {
  images: {
    loader: "imgix",
    path: "https://noop/",
  },
  // distDir: "../../dist/client",
  reactStrictMode: false,
};
