import React from "react";

function Challenge({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default Challenge;
